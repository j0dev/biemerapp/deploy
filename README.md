BiemerApp Deployment repository
======
This repository contains everything deployment related. From production setup trough container orchestration to local development setups.

This repository is split into 2 sections:

- Production setup

- Development setup

Please refer to to their individual README's and consult the [Documentation](https://biemerapp.j0dev.nl/)
