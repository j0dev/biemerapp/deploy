#!/bin/bash
# set -x
set -e

## Main variables
GIT_HOST="gitlab.com"
GIT_GROUP="j0dev/biemerapp"
GIT_REPOS=("deploy" "proxy" "backend" "control" "frontend" "frontend-override" "docs")

## Helper functions
function contains_argument() {
  ARG=$1
  shift
  for i in "$@"; do
    case $i in
      ${ARG})
        echo $i
        return 0
      ;;
    esac
  done
  return 1
}
function get_repo_prefix() {
  _=$(contains_argument --ssh "$@")
  if [[ "$?" -eq "0" ]]; then
    echo "git@${GIT_HOST}:${GIT_GROUP}"
    return
  else
    echo "https://${GIT_HOST}/${GIT_GROUP}"
    return
  fi
}

## Main logic // variables
GIT_PREFIX=$(get_repo_prefix "$@")

## Main logic // logic

### TODO check if dir is empty (ignoring the current executing script)
### TODO if dir is empty, clone here, or create subdir and cd
### TODO if dir is git repo, go up one above the repo and check if directory only contains deploy repo
### TODO if dir only contains deploy repo, set skip deploy and checkout all others

### Clone each repo
echo "> Cloning project repositories in the following directory:"
echo "> $(pwd)"
echo

for i in "${GIT_REPOS[@]}"; do
  git clone "${GIT_PREFIX}/${i}.git"
done

### TODO run development deploy setup / linking
./deploy/development/setup.sh
